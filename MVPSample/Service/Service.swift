//
//  Service.swift
//  MVPSample
//
//  Created by Le Quang Hung on 22/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation
class TrafficLightService {
    func getTrafficLight(colorName:(String), callBack:(TrafficLight?) -> Void) {
        let trafficLights = [TrafficLight(colorName: "Red", description: "Stop"),
                             TrafficLight(colorName: "Yellow", description: "Slow down"),
                             TrafficLight(colorName: "Green", description: "Go")]
        
        if let foundTrafficLight = trafficLights.first(where: {$0.colorName == colorName}) {
            callBack(foundTrafficLight)
        } else {
            callBack(nil)
        }
    }
}
