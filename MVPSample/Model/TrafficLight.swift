//
//  TrafficLight.swift
//  MVPSample
//
//  Created by Le Quang Hung on 22/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation

struct TrafficLight {
    var colorName: String?
    var description: String?
}
