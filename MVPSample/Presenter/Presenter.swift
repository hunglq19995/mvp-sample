//
//  Presenter.swift
//  MVPSample
//
//  Created by Le Quang Hung on 22/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import Foundation

protocol TrafficLightViewDelegate: NSObjectProtocol {
    func displayTrafficLight(description: (String))
}

class TrafficLightPresenter {
    private let trafficLightService: TrafficLightService
    weak var trafficLightViewDelegate: TrafficLightViewDelegate?
    
    init(trafficLightService: TrafficLightService) {
        self.trafficLightService = trafficLightService
    }
    
    func setViewDelegate(trafficLightViewDelegate: TrafficLightViewDelegate?) {
        self.trafficLightViewDelegate = trafficLightViewDelegate
    }
    
    func trafficLightSelected(colorName:(String)) {
        trafficLightService.getTrafficLight(colorName: colorName) { [weak self] traficLight in
            
            if let traficLight = traficLight {
                self?.trafficLightViewDelegate?.displayTrafficLight(description: traficLight.description ?? "")
            }
        }
    }
}
