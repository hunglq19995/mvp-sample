//
//  ViewController.swift
//  MVPSample
//
//  Created by Le Quang Hung on 22/02/2019.
//  Copyright © 2019 Le Quang Hung. All rights reserved.
//

import UIKit

class ViewController: UIViewController, TrafficLightViewDelegate {
    
    @IBOutlet weak var labelDescription: UILabel!
    private let trafficLightPresenter = TrafficLightPresenter(trafficLightService: TrafficLightService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        trafficLightPresenter.setViewDelegate(trafficLightViewDelegate: self)
    }
    
    @IBAction func buttonRed(_ sender: Any) {
        trafficLightPresenter.trafficLightSelected(colorName: "Red")
    }
    
    @IBAction func buttonYellow(_ sender: Any) {
        trafficLightPresenter.trafficLightSelected(colorName: "Yellow")
    }
    
    @IBAction func buttonGreen(_ sender: Any) {
        trafficLightPresenter.trafficLightSelected(colorName: "Green")
    }
    
    func displayTrafficLight(description: (String)) {
        labelDescription.text = description
    }

}

